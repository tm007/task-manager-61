package ru.tsc.apozdnov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.apozdnov.tm.listener.AbstractListener;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.DateUtil;

@Getter
@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @Autowired
    public IProjectEndpoint projectEndpoint;

    protected void showProject(@NotNull final ProjectDtoModel project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}