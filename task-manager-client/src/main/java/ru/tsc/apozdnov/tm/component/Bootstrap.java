package ru.tsc.apozdnov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.service.ILoggerService;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.api.service.ITokenService;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;
import ru.tsc.apozdnov.tm.service.TokenService;
import ru.tsc.apozdnov.tm.util.SystemUtil;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private FileScanner fileScanner;

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            publisher.publishEvent(new ConsoleEvent(command));
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        publisher.publishEvent(new ConsoleEvent(arg));
        return true;
    }

}