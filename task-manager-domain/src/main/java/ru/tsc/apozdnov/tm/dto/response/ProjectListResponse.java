package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;

import java.util.List;

@Getter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDtoModel> projects;

    public ProjectListResponse(@Nullable final List<ProjectDtoModel> projects) {
        this.projects = projects;
    }

}