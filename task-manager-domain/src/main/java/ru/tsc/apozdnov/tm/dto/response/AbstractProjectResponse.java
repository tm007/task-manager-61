package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDtoModel project;

    public AbstractProjectResponse(@Nullable final ProjectDtoModel project) {
        this.project = project;
    }

}