package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDtoModel task;

    public AbstractTaskResponse(@Nullable final TaskDtoModel task) {
        this.task = task;
    }

}