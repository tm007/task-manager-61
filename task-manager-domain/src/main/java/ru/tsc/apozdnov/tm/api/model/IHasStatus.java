package ru.tsc.apozdnov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}