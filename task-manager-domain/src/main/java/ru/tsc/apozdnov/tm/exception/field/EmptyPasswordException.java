package ru.tsc.apozdnov.tm.exception.field;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}